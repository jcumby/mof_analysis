{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using the Cambridge Structural Database (CSD)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [CSD](https://www.ccdc.cam.ac.uk/solutions/csd-core/components/csd/) contains structural (unit cell and atomic position) information for a large number of organic molecules and complexes, including a large number of metal organic frameworks (MOFs).\n",
    "\n",
    "In addition to the basic database of ~1,000,000 structures, the CSD also provides a number of tools for interacting with the structures, most notably:\n",
    "- Mercury\n",
    "    - For visualising structures and performing some simple calculations (such as porous volume)\n",
    "- Conquest\n",
    "    - For searching the database by e.g. structural moieties or functional groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Installing the CSD\n",
    "In order to use the CSD, you must first install it (UoE has a site licence for the software for use at home, and it is available on University-managed computers). Note that the full software requires ~20GB of storage space (but can later be removed).\n",
    "\n",
    "1. Navigate to https://www.ccdc.cam.ac.uk/support-and-resources/csdsdownloads/ and enter the customer ID and Activation Key you should have been provided with.\n",
    "    - You should shortly receive an email with download links:\n",
    "    \n",
    "    ![Email](CSD_download_email.png)\n",
    "\n",
    "    - You should download the \"CSDS_XXX_OS\" suitable for your computer (e.g. CSDS_2020.3_Windows)\n",
    "    - *Note:* The download is large (~10Gb) so may take a while depending on your internet connection\n",
    "2. Once downloaded, install the CSD on your computer accepting all of the defaults\n",
    "3. You should now be able to launch Mercury to start exploring!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using ConQuest\n",
    "[ConQuest](https://www.ccdc.cam.ac.uk/solutions/csd-core/components/conquest/) is the primary program for retrieving information from the CSD. Full details of its functions and tutorials on how to use it can be found on the [CCDC website](https://www.ccdc.cam.ac.uk/support-and-resources/CCDCResources/?rt=-1&mc=-1&p=0c7591ad-2201-e411-99f5-00505686f06e&so=0) but here we will give you a short overview of the main features."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once ConQuest is running, there are a number of ways of searching (or combining searches), for instance by Elements, Chemical formula, or by drawing structural features (e.g. functional groups). As an example, we'll consider finding all the MOFs containing copper.\n",
    "\n",
    "First, we need to select the 'Elements' search option:\n",
    "![Elements button](./conquest.png)\n",
    "In the resulting page we can either type element symbols (separated by spaces) or use the periodic table option.\n",
    "![Element search](./elements_search.png)\n",
    "\n",
    "At this point, clicking `Search` would run the query on the full database, but we want to restrict ourselves to 'ordered MOFs' suitable for calculations. To do this, press `Store` to save the query. If you want to combine multiple restrictions (e.g. Element + Year published) you can generate other criteria in the same way, storing each one.\n",
    "\n",
    "Once the criteria have been stored, you then need to click the main 'Search' button:\n",
    "![Search button](./search_queries.png)\n",
    "This will bring up a new dialogue which allows you to add further restrictions to the results, such as only structures where atomic coordinates are known. Press `Select subset` to restrict the subset searched. In the resulting window, we need to choose `Entries in a predefined hitlist`:\n",
    "![hitlist conquest](./hitlist_conquest.png)\n",
    "and in the drop-down box that appears select `non-disordered MOF subset`. Clicking `OK` will take you back to the main screen, where you can select `Start Search` to begin finding structures matching the criteria."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the search has completed, you should see the `View Results` tab, with a list of the matching REFCODES (6-letter code unique to each structure) down the right-hand side. Clicking a code will display a chemdraw-like representation of the structure in the main window.\n",
    "\n",
    "To use the search information to e.g. run a set of calculations on Eddie, you need to export the data. To do this you should select `File > Export parameters and data...` which will bring up another dialogue box where you can select the parameters you want to export (e.g. year published, compound name etc.). Under `File Type` you should select `Spreadsheet`, which you can read easily with Excel or Python+Pandas. Clicking `Save` will export the file to the location you choose, so you can then move it to Eddie.\n",
    "\n",
    "On Eddie, the most useful column in the file is the 'Refcode' which you can use to filter files from the Eddie folder containing different CIF files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using Mercury\n",
    "\n",
    "Mercury is designed for viewing structures, exporting images, and performing some quite sophisticated analysis of molecular geometry, crystal packing, etc. It is beyond the scope of this tutorial to show you all the features it contains; instead we suggest you read through the built-in tutorials (\"Help -> Tutorials -> ...\")\n",
    "\n",
    "The easiest way to view a structure in Mercury is if you already know its REFCODE (e,.g. from ConQuest); you can then search for that code in the \"Structure Navigator\" window to the right-hand side. Try adjusting the \"Display Options\" (bottom of the screen) to visualise either the individual molecules or their packing in space, as well as testing different viewing modes using the \"Style\" drop-down (top-left of the screen)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example - Calculating VOID volume\n",
    "\n",
    "As an example, we're going to calculate (and visualise) the internal void volume of a MOF using a tool within Mercury. The method is relatively simple:\n",
    "\n",
    "imagine moving a hard ball of a specified diameter across the crystal structure in small steps along x, y and z. At each point, if the ball does not overlap with the any of the atoms in the unit cell this point is considered a \"void\" (i.e. empty space). By doing this systematically, we can work out what fraction of the points are void space, and therefore how much of the structure is empty."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, open Mercury and search for the Refcode EDUSIF (the famous MOF-5). You might want to change the visualisation to make it look more like MOF5, in particular turning on \"Packing\" in the display options to show the full unit cell.\n",
    "\n",
    "Now, select \"Display -> Voids...\". This will bring up a dialogue to choose parameters such as the probe sphere radius and size of the step. For now just leave the defaults, and click \"Apply\"; this should show the edges of the void space as a surface, i.e. anything one side of the surface is void, and anything the other side is atoms.\n",
    "\n",
    "Play around with the parameters used, and see what effect it has on the results. Note that the structure contains solvent molecules, so you can't create the nice spherical hole we normally associate with MOF5!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using the Python API inside Mercury"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Although Mercury has some useful tools built in, sometimes we want to extend this with our own functions, or those written by others. For this reason, Mercury can use the built-in Python API of the CSD to run additional scripts to analyse structures. The intention is that your script should run on an individual structure you have manually selected (or perhaps comparing two); if you want to iterate over many structures you should use the full Python API interface (see other tutorial)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an introduction to the API, let's check that the Python environment in Mercury is working correctly. Click on \"CSD Python API -> Welcome.py\"; if everything is working correctly you should see a window pop up and, after a couple of seconds, the message\n",
    "\n",
    "> Welcome to the CSD Python API!\n",
    "\n",
    "If instead the window contains an error message, there is a problem with the API installation..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's try something a bit more useful; searching the CSD by name (you could also easily do this in ConQuest). Select \n",
    "`CSD Python API -> Searches -> Chemical_name_search.py` and in the window which pops up type `HKUST1`. After a few seconds, another window (\"Data Analysis\") should appear which contains a list of all CSD entries matching that name - currently, only DOTSOV44 appears for me. If you click on it, that will bring up the corresponding structure for you to view in Mercury.\n",
    "\n",
    "Note that name searching for MOFs is not particularly reliable, but hopefully this has shown you the power of the API in searching and manipulating structures!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
