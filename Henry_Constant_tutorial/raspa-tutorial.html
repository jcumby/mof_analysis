<hr>
<h1 id="calculating-henrys-coefficients-with-raspa2">Calculating Henry’s coefficients with RASPA2</h1>
<hr>
<p>C Hobday <a href="mailto:c.hobday@ed.ac.uk">c.hobday@ed.ac.uk</a><br>
M Lennox <a href="mailto:m.j.lennox@bath.ac.uk">m.j.lennox@bath.ac.uk</a><br>
G Donval &lt;<a href="mailto:g.donval@bath.ac.uk">g.donval@bath.ac.uk</a></p>
<p>In most adsorption applications, we are interested in the <em>capacity</em> of a material (i.e. how much sorbate it will adsorb) and the <em>affinity</em> of the material for a given species (i.e. how strongly the sorbate is adsorbed). This information is often presented in the form of an <em>adsorption isotherm</em> (i.e. how much gas is adsorbed in a material with respect to external pressure).</p>
<p>In later sessions, you will use MC simulations to predict and investigate the full adsorption isotherm. In this tutorial, you will focus on<br>
one particular way of quantifying the affinity of a material for a given<br>
species:<code>Henry</code>'s coefficient.</p>
<p>Objectives</p>
<ul>
<li>Becoming more familiar with Linux and the HPC facilities, running<br>
simulations using Raspa2 (a multipurpose MC code) and using VMD.</li>
<li>Acquiring a basic understanding of how Henry’s coefficient<br>
relates to the adsorption isotherms and to certain applications.</li>
<li>Beginning to explore how molecular-level information relates to the<br>
adsorption process and some of the characteristics and properties<br>
which influence adsorption in porous solids.</li>
</ul>
<p><strong>Before you start:</strong></p>
<ul>
<li>Review your Year 3 Statistical Mechanics lectures by Prof Camp.</li>
<li>Optionally read the introductory chapter in Rouquerol, <a href="https://www.sciencedirect.com/book/9780125989206/adsorption-by-powders-and-porous-solids"><em>Adsorption by Powders and Porous Solids</em></a> for a brief introduction to key concepts in adsorption.</li>
</ul>
<h2 id="background">Background</h2>
<p>You may have encountered Henry’s Law already in relation to the solubility of a gas in a liquid, or the volatility of a liquid.  In studies of adsorption, Henrys coefficient (K<sub>H</sub>) is an indicator of the <em>strength of interaction</em> between the sorbate and the adsorbent at infinite dilution (i.e.  when very few adsorbed molecules are present in the system). It is related to the slope of the initial, linear, portion of an isotherm as illustrated in Fig 1.  The steeper the slope, the greater the value of K<sub>H</sub> and the more strongly adsorbed the species is.</p>
<p><img src="https://drive.google.com/file/d/1x1XVsr-tjOQ41ps17gk9jDZ73uZKzwpO/view?usp=sharing" alt="Figure 1: Full adsorption isotherm as the pressure increases, the porous material’s gas uptake also increases; more gas is adsorbed at higher pressure. In the limit of vanishing pressures, the quantity of adsorbed gas is proportional to pressure itself. The slope of the resulting straight line is Henry’s coefficient"></p>
<p><img src="/Users/chobday/Documents/Teaching/2021-2022/4p/henry.png" alt="Figure 1: Full adsorption isotherm as the pressure increases, the porous material’s gas uptake also increases; more gas is adsorbed at higher pressure. In the limit of vanishing pressures, the quantity of adsorbed gas is proportional to pressure itself. The slope of the resulting straight line is Henry’s coefficient"><br>
K<sub>H</sub> is dependent on the temperature of the system as well as the compositionand characteristics of both the sorbate and adsorbent.  It is a usefulindicator for applications in which the low pressure (low loading) portion ofan isotherm is important, e.g. detecting or removing a harmful compound at lowconcentration in water (or in air).</p>
<p>The <em>selectivity</em> of a material for one compound over another (i.e. how easy it would be to separate two compounds via adsorption) can also be estimated from the ratio of their respective K<sub>H</sub> values.</p>
<p>In principle K<sub>H</sub> should be extracted from a full isotherm calculation. However at vanishing pressures the system is effectively in an infinite dilution state: the adsorbed gas molecules are so few that they don’t “see” each other. Their being non interacting can be efficiently leveraged using a specific MC technique, <code>Widom</code> insertion, that does not require a full isotherm calculation.</p>
<p>In this method, we are interested in evaluating the interaction energy of one adsorbed molecule with the adsorbent. The molecule is inserted at a random location in the simulation box, the interaction energy is calculated and the molecule is then removed. This is repeated for many thousands of trial points, allowing us to calculate the average interaction energy for the molecule-adsorbent system. Since we only considered one adsorbed molecule at any step, this is essentially adsorption at an extremely low loading, so can be related to the Henry’s coefficient via some thermodynamic relationships.</p>
<p>You will use <code>Widom</code> insertion in this tutorial to estimate K<sub>H</sub> for CO<sub>2</sub>, H<sub>2</sub>O and TNT in two different metal-organic frameworks (MOFs) at room temperature.</p>
<h2 id="preparing-the-simulation">Preparing the simulation</h2>
<p>Start by logging in on the cluster via the mobaxterm application (in windows <a href="https://mobaxterm.mobatek.net/download.html">click here to download</a>) or the terminal (Mac).<br>
<code>ssh &lt;your_username&gt;@eddie.ecdf.ed.ac.uk</code></p>
<p>Once logged in, typing <code>pwd</code> in the terminal will show your home path <code>/home/&lt;your_uun&gt;</code>. This home directory does not have much space for files, so we <strong>must</strong> change directory to our <strong>workspace</strong> space.</p>
<p>Change to workspace with:<br>
<code>$ cd /exports/csce/eddie/chem/groups/Chemistry4P/</code></p>
<p>Typing <code>ls</code> you will see that you each have your own folders in which you can work. Alongside your personal folders are a few others which contain the program RASPA which we will be using.</p>
<h2 id="using-raspa">Using RASPA</h2>
<p>The <strong>first</strong> time you use RASPA, we have to set up some variables within your Eddie account. You only need to do this once, and then they will be set for the whole time you do your project.</p>
<h3 id="create-a-conda-environment">Create a ‘conda’ environment</h3>
<p>Here we need to  set where conda will put your environments and your packages that you install.<br>
First we load anaconda and then create a place where we can access all our environments.</p>
<p>Make sure you are working in the path<br>
<code>/exports/csce/eddie/chem/groups/Chemistry4P/&lt;your_name&gt;/</code></p>
<p><code>module load anaconda</code><br>
<code>conda config --add envs_dirs /exports/csce/eddie/chem/groups/Chemistry4P/anaconda/envs</code></p>
<p>Now we have to tell it where to put our packages.<br>
Open up the <code>~/.condarc</code> file with:<br>
<code>nano ~/.condarc</code></p>
<p>Nano is a linux text editor, you can get some instructions on how to use it <a href="https://www.nano-editor.org/dist/latest/cheatsheet.html">here</a>.<br>
In a nutshell:</p>
<ul>
<li>you move around the text with the arrow keys.</li>
<li><code>ctrl + k</code> deletes a line</li>
<li><code>ctrl + x</code> exits the editor</li>
<li><code>y</code> then confirms if you want to save the file</li>
<li>press <code>enter</code> to confirm filename</li>
</ul>
<p>Delete all the text inside the file and copy in this text:<br>
<code>envs_dirs:</code><br>
<code>- /exports/csce/eddie/chem/groups/Chemistry4P/anaconda/envs</code><br>
<code>pkgs_dirs:</code><br>
<code>- /exports/csce/eddie/chem/groups/Chemistry4P/anaconda/pkgs</code></p>
<p>Now that these variables are set, you will only have to activate the python environment every time you want to use it.</p>
<h2 id="activating-the-python-environment">Activating the python environment</h2>
<p>Now that the computer knows where your environment is you can activate it to use it.<br>
<code>source activate 4P_python</code></p>
<p>You know if it’s loaded if you see the prompt:<br>
<code>(4P_python) [chobday@login01**(eddie)** Chemistry4P]$</code></p>
<h2 id="tutorial">Tutorial</h2>
<p>Make a raspa simulations folder within your own folder:<br>
<code>mkdir simulations</code></p>
<p>Change into this folder<br>
<code>$ cd simulations/</code></p>
<p>Make a new directory for your tutorials<br>
<code>$ mkdir tutorials</code></p>
<p>Download the required files from this link:<br>
<a href="https://drive.google.com/open?id=1SmcoqDqMBcrga57zRMwcGBOlcJrKTQ5R">IRMOF1.tar.xz</a><br>
copy them into that new <code>tutorials</code> folder and uncompress it it.</p>
<p>To do that,</p>
<ol>
<li>
<p>Open a <strong>new terminal</strong> in your mac, alongside your eddie terminal and navigate to your downloads folder and then “save copy” scp the tar file to the eddie hpc to the folder we want it to go to.</p>
<p><code>$ cd ~/User/&lt;your_username&gt;/Downloads/ $ scp IRMOF1.tar.xz &lt;your_uun&gt;@eddie.ecdf.ed.ac.uk:/exports/csce/eddie/chem/groups/Chemistry4P/&lt;your_name&gt;/simulations/tutorials/</code></p>
</li>
<li>
<p>Now, on the eddie terminal in the location <code>/exports/csce/eddie/chem/groups/Chemistry4P/&lt;your_name&gt;/simulations/tutorials/</code><br>
unzip the file with this command:<br>
<code>$ tar xf IRMOF1.tar.xz</code></p>
</li>
</ol>
<p>Have a look at the newly created folder, go into it and look into the files<br>
(using <code>more</code>):</p>
<p>e.g.<br>
<code>more CO2.def</code></p>
<ul>
<li><code>CO2.def</code>, <code>TNT.def</code> and <code>Tip5p.def</code> are all molecule definition files: they specify the types of atoms (or pseudo-atoms) present in the molecule and their relative positions. Why do you think  <code>TNT.def</code> only contains 16 ‘atom’ groups? Why do you think water  <code>Tip5p.def</code> contains five?</li>
<li><code>IRMOF1.cif</code> is also a molecule definition file. This one specifies the type and positions of atoms for IRMOF1.</li>
<li><code>pseudo_atoms.def</code> defines some properties of all of the atoms and pseudo-atoms used in the simulation. You won’t need to change anything inhere.</li>
<li><code>force_field_mixing_rules.def</code> and <code>force_field.def</code> define the force field parameters used for the simulation (<code>Lennard-Jones</code> in this case).</li>
<li><code>simulation.input</code> is what tells Raspa what type of simulation we want to carry out.</li>
</ul>
<p>This is the file we will be focusing on.</p>
<p>We greatly encourage you to inspect the structure of IRMOF1 on your laptop using <a href="https://www.ks.uiuc.edu/Research/vmd/">VMD</a> (with the .pdb file) or <a href="https://jp-minerals.org/vesta/en/download.html">Vesta</a> (which is free and can use format .pdb or .cif) or <a href="https://www.ccdc.cam.ac.uk/solutions/csd-system/components/mercury/">Mercury</a> (with either the .pdb or the .cif, but requires site license).<br>
We provide an illustration in Figure 2 but you should get a better feeling of what it really looks like if you can move it around.<br>
<img src="https://lh3.googleusercontent.com/RJw067WTgNuG-LI9SqYXnEa3ECyF-S7aAI0d1Hktun8qSwCSh7NT6Ku0rebye_glnlFybmKfnqC_" alt="enter image description here" title="Figure 2: IRMOF1"></p>
<p>Figure 2: Picture of IRMOF1: this is a porous metal-organic framework (MOF); it can accomodate molecules in its midst. How exactly it does so is captured in its adsorption isotherms, where the quantity of guest molecules is plotted against external pressure.</p>
<p>The file <code>simulation.input</code> can be divided into three parts.</p>
<p>Lines 1 to 8 in <code>simulation.input</code> correspond to the first part,<br>
where you must define the kind of simulation you want to carry out.  The most important parameters are <code>NumberOfCycles</code> which is related to the number of MC moves we will perfom: the higher the number, the longer the simulation;<code>ChargeMethod</code> which is set to <code>None</code> meaning we are ignoring electrostatic interactions and <code>CutOffVDW</code> which defines a cut-off radius of<code>12.8 Angstroms</code> for LJ interactions. At the moment, we have chosen  a nonsensically small number of cycles: this is going to be changed when we run our first calculations.<br>
Now think about the molecules we are simulating: do you think electrostatics are important in these cases?<br>
Why do you think we have ignored them for this workshop?</p>
<h3 id="simulation.input">simulation.input</h3>
<p>SimulationType                MonteCarlo<br>
NumberOfCycles                2<br>
NumberOfInitializationCycles  0<br>
PrintEvery                    1000<br>
PrintPropertiesEvery          1000<br>
Forcefield                    raspa<br>
ChargeFromChargeEquilibration Yes<br>
ChargeMethod                  None<br>
CutOffVDW                     12.8</p>
<p>Framework 0<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; FrameworkName                 IRMOF-1<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; RemoveAtomNumberCodeFromLabel no<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; UnitCells                     1 1 1<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  ExternalTemperature           298.0</p>
<p>Component 0 &nbsp;&nbsp;MoleculeName                  CO2<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;    MoleculeDefinition            local<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;IdealRosenbluthValue          1.0<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;WidomProbability              1.0<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;CreateNumberOfMolecules       0</p>
<p>Component 1&nbsp;&nbsp;     MoleculeName                  TNT<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;MoleculeDefinition            local<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;IdealRosenbluthValue          1.0<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;WidomProbability              1.0<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;CreateNumberOfMolecules       0</p>
<p>Component 2   &nbsp;&nbsp;MoleculeName                  Tip5p<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;MoleculeDefinition            local<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;IdealRosenbluthValue          1.0<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;WidomProbability              1.0<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;CreateNumberOfMolecules       0</p>
<p>The second part starts on line 10 with the definition of the porous framework <code>Framework 0</code> wherein we are going to do our calculations. We told Raspa to use the structure given in <code>IRMOF-1.cif</code> and to thermalise it at <code>298.0 K</code>. IRMOF1 is a crystal: it is defined in term of a repeating unit called a <em>unit-cell</em>. That unit-cell is actually a box: by specifying <code>UnitCells 1 1 1</code> we are simply telling Raspa that our whole simulation box should be as big as a single unit-cell along the <code>x</code>, <code>y</code> and <code>z</code> directions (we will come back to this later).</p>
<p>The third part starts on line 16 in this example and encompasses all the gas molecules <code>Component n</code> that we want to use for <code>Widom</code> insertion. The <code>MoleculeName</code> refer to each respective <code>.def</code> file.<br>
For a usual calculation, specifying multiple components at once like this would mean we want to study a mixture of them. However because <code>Widom</code> insertion does not actually insert anything, we can evaluate K<sub>H</sub> for all three components at once!</p>
<h2 id="running-the-simulation-for-irmof1">Running the simulation for IRMOF1</h2>
<p>We remind you that running simulations on EDDIE directly in the command line, is <strong>strictly forbidden</strong>. When you are on  EDDIE, you log onto the <em>front node</em>. This is used for general accessing of files and folders, but not anything which requires great computing power.<br>
You also need to work in your ‘work’ space as this has lots of memory to store your data.</p>
<p>We will</p>
<ol>
<li>
<p>Work in the <strong>WORK</strong> space, in the folder you created<br>
<code>$ cd/exports/csce/eddie/chem/groups/Hobday/Jiaying/simulations/tutorials</code></p>
</li>
<li>
<p>Edit <code>simulation.input</code> and set the number of cycles to 20, save and close the file.</p>
</li>
<li>
<p>Run the simulation by submitting the “job” to the <em>computing nodes</em> using a script that we provided called “submit.job”:<br>
Let’s have a look at what the submission script does:</p>
<p><code>#!/bin/sh</code><br>
<code># Grid Engine options (lines prefixed with #$)</code><br>
<code>#$ -N raspa</code><br>
<code>#$ -cwd</code><br>
<code>#$ -l h_rt=00:10:00</code><br>
<code>#$ -l h_vmem=2G</code><br>
<code>#$ -pe mpi 16</code><br>
<code># These options are:</code><br>
<code># job name: -N</code><br>
<code># use the current working directory: -cwd</code><br>
<code># runtime limit of</code> <code>10</code>  <code>mins: -l h_rt</code><br>
<code># memory limit of</code> <code>2</code>  <code>Gbyte: -l h_vmem</code><br>
<code># parallel environment and no. of slots: -pe</code><br>
<code># Initialise the environment modules</code><br>
<code>. /etc/profile.d/modules.sh</code><br>
<code># Load modules</code><br>
<code>module load anaconda</code><br>
<code>source activate mypython</code><br>
<code># Run the program</code><br>
<code>simulate simulation.input</code></p>
</li>
</ol>
<p>To use it:<br>
<code>$ qsub submit.job</code><br>
Now lets have a look if your job has been submitted.<br>
<code>$ qstat</code></p>
<p>This shows the time, name and state of your calculation.<br>
q means queueing<br>
r means running</p>
<p>It might take a while to be first in the queue to run, but the calculation itself is quick and should  last a few seconds.<br>
Once it is finished, you will see several new directories. The simulation results are stored in <code>Output/System_0/</code>. Use <code>ls</code> to see which file is in there. That file is quite big. We are going to use <code>grep</code> to filter out the output file and only display Henry’s coefficients:</p>
<pre><code>$ grep "Average Henry" Output/System_0/output_*
Average Henry coefficient:
  [CO2] Average Henry coefficient: XXX +/- XXX
  [TNT] Average Henry coefficient: XXX +/- XXX
  [Tip5p] Average Henry coefficient: XXX +/- XXX
</code></pre>
<p>We can do the same for adsorption energies, the average strength of<br>
interaction between the compound and the MOF:</p>
<pre><code>$ grep "kJ/mol)" Output/System_0/output_*
</code></pre>
<ul>
<li>Do the same calculation for each of these numbers of cycles:<br>
20, 100, 200, 1000, 2000.</li>
<li>Note down K<sub>H</sub> and the adsorption energies for each of them.</li>
<li>Also appreciate how, as you increase the number of cycles, the calculation takes longer and longer to terminate.</li>
<li>Which K<sub>H</sub> exhibits the most variation? Why?</li>
<li>Have a look at the adsorption energies. Did you expect the interaction energy to be negative or positive for adsorption?</li>
</ul>
<p><em>NOTE:</em> For long file names such as the one used by Raspa for its output, it is really convenient to get some help from the prompt instead of copy-pasting the output of <code>ls</code>. One trick is to use <code>Tab</code> to auto-complete the current file name (one for auto-completion, two for displaying matching file names):</p>
<pre><code>    $ grep "Average Henry" Output/System_0/out{Tab}{Tab}

The other way (used above) is to type a ``*`` (pronounced *wildcard*)
to tell the prompt to provide the program with all inputs matching (i.e. starting with) :`Output/System_0/output_`.
</code></pre>
<h2 id="running-the-simulation-for-uio-66">Running the simulation for UiO-66</h2>
<h3 id="uio-66">UiO-66</h3>
<p>Now let’s try calculate Henry’s coefficients for the same molecules in a<br>
different MOF UiO66. Download the <code>.cif</code> file from here:<br>
<a href="https://drive.google.com/open?id=1J5dy4ad3S_KgOe_WPtXVwms3PZFPe3NU">UiO66.tar.xz</a>. Copy it over to <code>/exports/csce/eddie/chem/groups/Hobday/Jiaying/simulations/tutorials/</code> (like you did for IRMOF-1). Decompress it in the tutorial folder with:</p>
<pre><code>$ tar xf UiO66.tar.xz
</code></pre>
<p>Again, use VMD or Mercury to familiarise yourself with this new structure. A picture of it is given in Figure 3 but you should use the viewer to understand its pore geometry.<br>
<img src="https://lh3.googleusercontent.com/OxT8Hbw-B2OF-YdZQQPMRTVHYeOcmZiUAPdx-mUgm2uO_LeUNtChP6p1L5QxlYg-1CZzcn8hJ_5h" alt="enter image description here" title="UiO-66: just like IRMOF1, it is a porous MOF. Note its different pore geometry from IRMOF1"></p>
<p>Figure 3: UiO66: just like IRMOF1, this is a porous metal-organic framework; it can accomodate molecules in its midst. Note how different its pore geometry is from IRMOF1.</p>
<p>This time you only have the <code>.cif</code> file at your disposal. You could<br>
type in everything from scratch, but we suggest instead that you copy over the files from your previous calculations in the current folder. You can use the <em>wildcard</em> pattern (*) discussed above to select all the files from<br>
<code>/exports/csce/eddie/chem/groups/Hobday/Jiaying/tutorials/IRMOF1/</code> at once.</p>
<p>What needs to change in <code>simulation.input</code>? Be sure to use the correct<br>
framework name.</p>
<p>There is one other thing that needs to be changed. Like IRMOF1, the unit-cell of UiO66 is cubic but it is only <code>20.7 Angstroms</code> along each edge<br>
(IRMOF1 has edge lengths of <code>25.8 Angstroms</code>). We are using a cut-off<br>
radius of <code>12.8 Angstroms</code>. Is a simulation box of <code>1*1*1</code> UiO66 unit-cell big enough ?</p>
<p>If this box size was used, we would disobey the <em>minimum image convention</em>.<br>
This is a convention which stops particles interacting with copies of themselves. So the cut-off distance is always less than half the length of the box.</p>
<p>See here for more on the  <a href="https://drive.google.com/open?id=1nOPBDGAcufRW0RS70jnDSkuoM2HFJNzb"><em>minimum image convention</em></a></p>
<p>Should you increase the number of unit-cells or should you decrease the cut-off radius?</p>
<p>Once you are confident about the changes you made, be absolutely certain to still be on the computing node and launch the calculation as described before. Once finished, make a note of the average K<sub>H</sub> and interaction energy for each compound.</p>
<h2 id="analysing-the-data">Analysing the data</h2>
<ul>
<li>
<p>You should start by comparing the average K<sub>H</sub> obtained for each compound in IRMOF1: which one is the most strongly adsorbed? how does this relate to adsorption energies?</p>
</li>
<li>
<p>Take each compound one by one and compare adsorption energies for each  framework: is one framework more strongly-interacting than the other?  What about K<sub>H</sub>? One of the most important factors in adsorption in <em>microporous</em> adsorbents (solids which have pores <code>&lt; 20 Angstroms</code> in diameter) like IRMOF1 and UiO66 is the pore size. Generally, molecules are more strongly adsorbed in smaller pores. Why do you think this is the case? Do your results agree with this general trend?</p>
<p>To answer these questions, you need to estimate pore sizes. For the time being, you can get a fairly good estimation of it in VMD by measuring the distance between atoms using the <code>Mouse --&gt; Label --&gt; Bonds</code> menu and by clicking on any two atoms to get a distance (even if they are non-bonded to each other).  If you chose appropriate atoms, you should be able to get a rough estimate of pore diameters for each structure. In addition, you can use Mercury’s void tool <code>Display --&gt; Voids --&gt; Ok</code> to know the pore volume and visualise it.</p>
</li>
<li>
<p>In most applications, more than one compound will be present in the process. For both CO<sub>2</sub> capture (where we try to remove CO<sub>2</sub> from flue gases) and TNT detection, water will be present in the process. We want to selectively adsorb CO<sub>2</sub> or TNT and adsorb as little water as possible. You can use Henry’s coefficients to estimate the selectivity S<sub>ab</sub> between species<code>a</code> and <code>b</code> at low loading:</p>
<p><span class="katex--inline"><span class="katex"><span class="katex-mathml"><math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><mrow><msub><mi>S</mi><mrow><mi mathvariant="double-struck">a</mi><mi mathvariant="double-struck">b</mi></mrow></msub><mo>≈</mo><mfrac><msubsup><mi>K</mi><mi mathvariant="normal">H</mi><mi mathvariant="normal">a</mi></msubsup><msubsup><mi>K</mi><mi mathvariant="normal">H</mi><mi mathvariant="normal">b</mi></msubsup></mfrac></mrow><annotation encoding="application/x-tex">S_\mathbb{ab} \approx \frac{K_\mathrm{H}^\mathrm{a}}{K_\mathrm{H}^\mathrm{b}}</annotation></semantics></math></span><span class="katex-html" aria-hidden="true"><span class="base"><span class="strut" style="height: 0.83333em; vertical-align: -0.15em;"></span><span class="mord"><span class="mord mathnormal" style="margin-right: 0.05764em;">S</span><span class="msupsub"><span class="vlist-t vlist-t2"><span class="vlist-r"><span class="vlist" style="height: 0.33610799999999996em;"><span class="" style="top: -2.5500000000000003em; margin-left: -0.05764em; margin-right: 0.05em;"><span class="pstrut" style="height: 2.7em;"></span><span class="sizing reset-size6 size3 mtight"><span class="mord mtight"><span class="mord mathnormal mtight">ab</span></span></span></span></span><span class="vlist-s">​</span></span><span class="vlist-r"><span class="vlist" style="height: 0.15em;"><span class=""></span></span></span></span></span></span><span class="mspace" style="margin-right: 0.2777777777777778em;"></span><span class="mrel">≈</span><span class="mspace" style="margin-right: 0.2777777777777778em;"></span></span><span class="base"><span class="strut" style="height: 1.663265em; vertical-align: -0.6360849999999999em;"></span><span class="mord"><span class="mopen nulldelimiter"></span><span class="mfrac"><span class="vlist-t vlist-t2"><span class="vlist-r"><span class="vlist" style="height: 1.02718em;"><span class="" style="top: -2.60142em;"><span class="pstrut" style="height: 3em;"></span><span class="sizing reset-size6 size3 mtight"><span class="mord mtight"><span class="mord mtight"><span class="mord mathnormal mtight" style="margin-right: 0.07153em;">K</span><span class="msupsub"><span class="vlist-t vlist-t2"><span class="vlist-r"><span class="vlist" style="height: 0.8408285714285714em;"><span class="" style="top: -2.160707142857143em; margin-left: -0.07153em; margin-right: 0.07142857142857144em;"><span class="pstrut" style="height: 2.5em;"></span><span class="sizing reset-size3 size1 mtight"><span class="mord mathrm mtight">H</span></span></span><span class="" style="top: -2.8448em; margin-right: 0.07142857142857144em;"><span class="pstrut" style="height: 2.5em;"></span><span class="sizing reset-size3 size1 mtight"><span class="mord mathrm mtight">b</span></span></span></span><span class="vlist-s">​</span></span><span class="vlist-r"><span class="vlist" style="height: 0.3392928571428572em;"><span class=""></span></span></span></span></span></span></span></span></span><span class="" style="top: -3.23em;"><span class="pstrut" style="height: 3em;"></span><span class="frac-line" style="border-bottom-width: 0.04em;"></span></span><span class="" style="top: -3.5102em;"><span class="pstrut" style="height: 3em;"></span><span class="sizing reset-size6 size3 mtight"><span class="mord mtight"><span class="mord mtight"><span class="mord mathnormal mtight" style="margin-right: 0.07153em;">K</span><span class="msupsub"><span class="vlist-t vlist-t2"><span class="vlist-r"><span class="vlist" style="height: 0.7385428571428572em;"><span class="" style="top: -2.214em; margin-left: -0.07153em; margin-right: 0.07142857142857144em;"><span class="pstrut" style="height: 2.5em;"></span><span class="sizing reset-size3 size1 mtight"><span class="mord mathrm mtight">H</span></span></span><span class="" style="top: -2.931em; margin-right: 0.07142857142857144em;"><span class="pstrut" style="height: 2.5em;"></span><span class="sizing reset-size3 size1 mtight"><span class="mord mathrm mtight">a</span></span></span></span><span class="vlist-s">​</span></span><span class="vlist-r"><span class="vlist" style="height: 0.286em;"><span class=""></span></span></span></span></span></span></span></span></span></span><span class="vlist-s">​</span></span><span class="vlist-r"><span class="vlist" style="height: 0.6360849999999999em;"><span class=""></span></span></span></span></span><span class="mclose nulldelimiter"></span></span></span></span></span></span></p>
<p>A selectivity of greater than <strong>1</strong> indicates that component a will be preferentially adsorbed. Based on your results, which MOF is a better<br>
choice for use in a carbon capture process? What about TNT detection?</p>
</li>
<li>
<p>What about equilibration in this case? How many values should we remove to get a good sample?</p>
</li>
</ul>
<h2 id="going-further">Going further</h2>
<p>In this tutorial we have used the <code>TraPPE</code> (transferable potentials for phase equilibria) models for CO<sub>2</sub> and TNT. These parameters are fitted against experimental <em>vapour-liquid equilibria</em> data in the case of CO<sub>2</sub>. For TNT, those data don’t exist and so the parameters are those developed for similar compounds.</p>
<ul>
<li>note: It is <em>always</em> a good idea (read, <em>necessary</em>) to identify limitations in your simulation approach!</li>
</ul>
<p>Instead of the <code>spc</code> water model you used earlier, we are using a slightly more complicated model (<code>tip5p</code>). The dispersion interactions are still modelled using a <strong>LJ potential</strong> based solely on oxygen–oxygen interactions, but there are some extra pseudo-atoms (called <code>Lw</code> in this simulation) with electrostatic contributions to try and model the hydrogen bonding more effectively than <code>spc</code> water.  For more detail on the concepts, thermodynamics and maths behind Henry’s constants as applied to adsorption, you may find Rouquerol’s <a href="https://www.sciencedirect.com/book/9780125989206/adsorption-by-powders-and-porous-solids">Adsorption by powders and porous solids</a> helpful, as well as Myers <a href="https://www.seas.upenn.edu/~amyers/CHAPTER21.pdf">Thermodynamics of Adsorption</a>  (though it is a bit more heavy-going than the<br>
former!!).</p>
<blockquote>
<p>Written with <a href="https://stackedit.io/">StackEdit</a>.</p>
</blockquote>

