﻿
***********************************************
# Calculating Henry's coefficients with RASPA2
***********************************************

C Hobday <c.hobday@ed.ac.uk><br>
M Lennox <m.j.lennox@bath.ac.uk><br>
G Donval <g.donval@bath.ac.uk

In most adsorption applications, we are interested in the *capacity* of a material (i.e. how much sorbate it will adsorb) and the *affinity* of the material for a given species (i.e. how strongly the sorbate is adsorbed). This information is often presented in the form of an *adsorption isotherm* (i.e. how much gas is adsorbed in a material with respect to external pressure).

In later sessions, you will use MC simulations to predict and investigate the full adsorption isotherm. In this tutorial, you will focus on
one particular way of quantifying the affinity of a material for a given
species:`Henry`'s coefficient.

## Objectives


   * Becoming more familiar with Linux and the HPC facilities, running 
      simulations using Raspa2 (a multipurpose MC code) and using VMD.
   * Acquiring a basic understanding of how Henry's coefficient 
      relates to the adsorption isotherms and to certain applications.
   * Beginning to explore how molecular-level information relates to the 
      adsorption process and some of the characteristics and properties 
      which influence adsorption in porous solids.

**Before you start:**

- Review your Year 3 Statistical Mechanics lectures by Prof Camp.
- Optionally read the introductory chapter in Rouquerol, [_Adsorption by Powders and Porous Solids_](https://www.sciencedirect.com/book/9780125989206/adsorption-by-powders-and-porous-solids) for a brief introduction to key concepts in adsorption.


## Background

You may have encountered Henry's Law already in relation to the solubility of a gas in a liquid, or the volatility of a liquid.  In studies of adsorption, Henrys coefficient (K<sub>H</sub>) is an indicator of the *strength of interaction* between the sorbate and the adsorbent at infinite dilution (i.e.  when very few adsorbed molecules are present in the system). It is related to the slope of the initial, linear, portion of an isotherm as illustrated in Fig 1.  The steeper the slope, the greater the value of K<sub>H</sub> and the more strongly adsorbed the species is. 


![Figure 1: Full adsorption isotherm as the pressure increases, the porous material’s gas uptake also increases; more gas is adsorbed at higher pressure. In the limit of vanishing pressures, the quantity of adsorbed gas is proportional to pressure itself. The slope of the resulting straight line is Henry’s coefficient](./images/henry.png) 


K<sub>H</sub> is dependent on the temperature of the system as well as the compositionand characteristics of both the sorbate and adsorbent.  It is a usefulindicator for applications in which the low pressure (low loading) portion ofan isotherm is important, e.g. detecting or removing a harmful compound at lowconcentration in water (or in air). 

The *selectivity* of a material for one compound over another (i.e. how easy it would be to separate two compounds via adsorption) can also be estimated from the ratio of their respective K<sub>H</sub> values.  

In principle K<sub>H</sub> should be extracted from a full isotherm calculation. However at vanishing pressures the system is effectively in an infinite dilution state: the adsorbed gas molecules are so few that they don't "see" each other. Their being non interacting can be efficiently leveraged using a specific MC technique, `Widom` insertion, that does not require a full isotherm calculation.

In this method, we are interested in evaluating the interaction energy of one adsorbed molecule with the adsorbent. The molecule is inserted at a random location in the simulation box, the interaction energy is calculated and the molecule is then removed. This is repeated for many thousands of trial points, allowing us to calculate the average interaction energy for the molecule-adsorbent system. Since we only considered one adsorbed molecule at any step, this is essentially adsorption at an extremely low loading, so can be related to the Henry's coefficient via some thermodynamic relationships.

You will use `Widom` insertion in this tutorial to estimate K<sub>H</sub> for CO <sub>2</sub>, H <sub>2</sub>O and TNT in two different metal-organic frameworks (MOFs) at room temperature.


## Preparing the simulation


Start by logging in on the cluster via the mobaxterm application (in windows [click here to download](https://mobaxterm.mobatek.net/download.html)) or the terminal (Mac).
`ssh <your_username>@eddie.ecdf.ed.ac.uk`

Once logged in, typing `pwd` in the terminal will show your home path `/home/<your_uun>`. This home directory does not have much space for files, so we **must** change directory to our **workspace** space. 



Change to workspace with:
``` bash
cd /exports/csce/eddie/chem/groups/Chemistry4P/
```

Typing `ls` you will see that you each have your own folders in which you can work. Alongside your personal folders are a few others which contain the program RASPA which we will be using. 

## Using RASPA

The **first** time you use RASPA, we have to set up some variables within your Eddie account. You only need to do this once, and then they will be set for the whole time you do your project. 


### Create a 'conda' environment

Here we need to  set where conda will put your environments and your packages that you install. 
First we load anaconda and then create a place where we can access all our environments.

Make sure you are working in the path
`/exports/csce/eddie/chem/groups/Chemistry4P/<your_name>/` and then type:

```
module load anaconda
conda config --add envs_dirs /exports/csce/eddie/chem/groups/Chemistry4P/anaconda/envs
```

Now we have to tell it where to put our packages.
Open up the `~/.condarc` file with:
```
nano ~/.condarc
```

Nano is a linux text editor, you can get some instructions on how to use it [here](https://www.nano-editor.org/dist/latest/cheatsheet.html).
In a nutshell:
- you move around the text with the arrow keys.
- `ctrl + k` deletes a line
-  `ctrl + x` exits the editor
-  `y` then confirms if you want to save the file
- press `enter` to confirm filename


Delete all the text inside the file and copy in this text:
```
envs_dirs:
 - /exports/csce/eddie/chem/groups/Chemistry4P/anaconda/envs
pkgs_dirs:
 - /exports/csce/eddie/chem/groups/Chemistry4P/anaconda/pkgs
 ```

Now that these variables are set, you will only have to activate the python environment every time you want to use it. 

## Activating the python environment

Now that the computer knows where your environment is you can activate it to use it. 
`source activate 4P_python`

You know if it's loaded if you see the prompt:
`(4P_python) [chobday@login01**(eddie)** Chemistry4P]$`


## Tutorial

Make a raspa simulations folder within your own folder:
`mkdir simulations`

Change into this folder
`cd simulations/`

Make a new directory for your tutorials
`mkdir tutorials`

Download the required files from this link: 
[IRMOF1.tar.xz](https://drive.google.com/open?id=1SmcoqDqMBcrga57zRMwcGBOlcJrKTQ5R)
copy them into that new `tutorials` folder and uncompress it it. 

To do that, 
1. Open a **new terminal** in your mac, alongside your eddie terminal and navigate to your downloads folder and then "save copy" scp the tar file to the eddie hpc to the folder we want it to go to.

    ```bash
    cd ~/User/<your_username>/Downloads/
    scp IRMOF1.tar.xz <your_uun>@eddie.ecdf.ed.ac.uk:/exports/csce/eddie/chem/groups/Chemistry4P/<your_name>/simulations/tutorials/
    ```
2. Now, on the eddie terminal in the location `/exports/csce/eddie/chem/groups/Chemistry4P/<your_name>/simulations/tutorials/`
unzip the file with this command:
   `tar xf IRMOF1.tar.xz`


Have a look at the newly created folder, go into it and look into the files
(using ``more``):

e.g. 
` more CO2.def `

* `CO2.def`, `TNT.def` and `Tip5p.def` are all molecule definition files: they specify the types of atoms (or pseudo-atoms) present in the molecule and their relative positions. Why do you think  `TNT.def` only contains 16 'atom' groups? Why do you think water  `Tip5p.def` contains five?
* `IRMOF1.cif` is also a molecule definition file. This one specifies the type and positions of atoms for IRMOF1.
* `pseudo_atoms.def` defines some properties of all of the atoms and pseudo-atoms used in the simulation. You won't need to change anything inhere.
* `force_field_mixing_rules.def` and `force_field.def` define the force field parameters used for the simulation (`Lennard-Jones` in this case).
* `simulation.input` is what tells Raspa what type of simulation we want to carry out. 

This is the file we will be focusing on.

We greatly encourage you to inspect the structure of IRMOF1 on your laptop using [VMD](https://www.ks.uiuc.edu/Research/vmd/) (with the .pdb file) or [Vesta](https://jp-minerals.org/vesta/en/download.html) (which is free and can use format .pdb or .cif) or [Mercury](https://www.ccdc.cam.ac.uk/solutions/csd-system/components/mercury/) (with either the .pdb or the .cif, but requires site license). 
We provide an illustration in Figure 2 but you should get a better feeling of what it really looks like if you can move it around.
![enter image description here](https://lh3.googleusercontent.com/RJw067WTgNuG-LI9SqYXnEa3ECyF-S7aAI0d1Hktun8qSwCSh7NT6Ku0rebye_glnlFybmKfnqC_ "Figure 2: IRMOF1")

Figure 2: Picture of IRMOF1: this is a porous metal-organic framework (MOF); it can accomodate molecules in its midst. How exactly it does so is captured in its adsorption isotherms, where the quantity of guest molecules is plotted against external pressure.



The file `simulation.input` can be divided into three parts.  

Lines 1 to 8 in `simulation.input` correspond to the first part, 
where you must define the kind of simulation you want to carry out.  The most important parameters are ``NumberOfCycles`` which is related to the number of MC moves we will perfom: the higher the number, the longer the simulation;``ChargeMethod`` which is set to ``None`` meaning we are ignoring electrostatic interactions and ``CutOffVDW`` which defines a cut-off radius of`12.8 Angstroms` for LJ interactions. At the moment, we have chosen  a nonsensically small number of cycles: this is going to be changed when we run our first calculations. 
Now think about the molecules we are simulating: do you think electrostatics are important in these cases? 
Why do you think we have ignored them for this workshop?

### simulation.input

    SimulationType                MonteCarlo
    NumberOfCycles                2
    NumberOfInitializationCycles  0
    PrintEvery                    1000
    PrintPropertiesEvery          1000
    Forcefield                    raspa
    ChargeFromChargeEquilibration Yes
    ChargeMethod                  None
    CutOffVDW                     12.8

    Framework 0 
                FrameworkName                 IRMOF-1
                RemoveAtomNumberCodeFromLabel no
                UnitCells                     1 1 1
                ExternalTemperature           298.0

    Component 0 MoleculeName                  CO2
                MoleculeDefinition            local
                IdealRosenbluthValue          1.0
                WidomProbability              1.0
                CreateNumberOfMolecules       0

    Component 1 MoleculeName                  TNT
                MoleculeDefinition            local
                IdealRosenbluthValue          1.0
                WidomProbability              1.0
                CreateNumberOfMolecules       0

    Component 2 MoleculeName                  Tip5p
                MoleculeDefinition            local
                IdealRosenbluthValue          1.0
                WidomProbability              1.0
                CreateNumberOfMolecules       0


The second part starts on line 10 with the definition of the porous framework ``Framework 0`` wherein we are going to do our calculations. We told Raspa to use the structure given in `IRMOF-1.cif` and to thermalise it at `298.0 K`. IRMOF1 is a crystal: it is defined in term of a repeating unit called a *unit-cell*. That unit-cell is actually a box: by specifying ``UnitCells  1 1 1`` we are simply telling Raspa that our whole simulation box should be as big as a single unit-cell along the `x`, `y` and `z` directions (we will come back to this later).

The third part starts on line 16 in this example and encompasses all the gas molecules ``Component n`` that we want to use for `Widom` insertion. The ``MoleculeName`` refer to each respective `.def` file.  
For a usual calculation, specifying multiple components at once like this would mean we want to study a mixture of them. However because `Widom` insertion does not actually insert anything, we can evaluate K<sub>H</sub> for all three components at once!


Running the simulation for IRMOF1
------------------------------------

We remind you that running simulations on EDDIE directly in the command line, is **strictly forbidden**. When you are on  EDDIE, you log onto the *front node*. This is used for general accessing of files and folders, but not anything which requires great computing power.  
You also need to work in your 'work' space as this has lots of memory to store your data. 

 We will 
 1. Work in the **WORK** space, in the folder you created
    ```
    cd/exports/csce/eddie/chem/groups/chem4p/<name>/simulations/tutorials
    ```
 2. Edit `simulation.input` and set the number of cycles to 20, save and close the file. 
 3. Run the simulation by submitting the "job" to the *computing nodes* using a script that we provided called "submit.job":
Let's have a look at what the submission script does:

    ```
    #!/bin/sh
    # Grid Engine options (lines prefixed with #$)
    #$ -N raspa
    #$ -cwd
    #$ -l h_rt=00:10:00
    #$ -l h_vmem=2G
    #$ -pe mpi 16
    # These options are:
    # job name: -N
    # use the current working directory: -cwd
    # runtime limit of 10 mins: -l h_rt
    # memory limit of 2 GByte: -l h_vmem
    # parallel environment and no. of slots: -pe
    # Initialise the environment modules
    . /etc/profile.d/modules.sh
    # Load modules
    module load anaconda
    source activate mypython
    # Run the program
    simulate simulation.input
    ```
  
To use it:
```
qsub submit.job
```

Now lets have a look if your job has been submitted.
```
qstat
```

This shows the time, name and state of your calculation. 
- `q` means queueing
- `r` means running

It might take a while to be first in the queue to run, but the calculation itself is quick and should  last a few seconds.
 Once it is finished, you will see several new directories. The simulation results are stored in `Output/System_0/`. Use ``ls`` to see which file is in there. That file is quite big. We are going to use ``grep`` to filter out the output file and only display Henry's coefficients:
 
 ```bash
 grep "Average Henry" Output/System_0/output_*
 ```
 Outputs:

    Average Henry coefficient:
      [CO2] Average Henry coefficient: XXX +/- XXX
      [TNT] Average Henry coefficient: XXX +/- XXX
      [Tip5p] Average Henry coefficient: XXX +/- XXX

We can do the same for adsorption energies, the average strength of
interaction between the compound and the MOF:

``` bash
grep "kJ/mol)" Output/System_0/output_*
```

* Do the same calculation for each of these numbers of cycles: 
20, 100, 200, 1000, 2000. 
* Note down K<sub>H</sub> and the adsorption energies for each of them. 
* Also appreciate how, as you increase the number of cycles, the calculation takes longer and longer to terminate.
*  Which K<sub>H</sub> exhibits the most variation? Why?
* Have a look at the adsorption energies. Did you expect the interaction energy to be negative or positive for adsorption?



*NOTE:* For long file names such as the one used by Raspa for its output, it is really convenient to get some help from the prompt instead of copy-pasting the output of ``ls``. One trick is to use `Tab` to auto-complete the current file name (one for auto-completion, two for displaying matching file names):

```bash
grep "Average Henry" Output/System_0/out{Tab}{Tab}
```

The other way (used above) is to type a ``*`` (pronounced *wildcard*) to tell the prompt to provide the program with all inputs matching (i.e. starting with):
``` bash
grep "Average Henry" Output/System_0/output_*
``````


## Running the simulation for UiO-66

### UiO-66

Now let's try calculate Henry's coefficients for the same molecules in a
different MOF UiO66. Download the `.cif` file from here:
[UiO66.tar.xz](https://drive.google.com/open?id=1J5dy4ad3S_KgOe_WPtXVwms3PZFPe3NU). Copy it over to `/exports/csce/eddie/chem/groups/Hobday/Jiaying/simulations/tutorials/` (like you did for IRMOF-1). Decompress it in the tutorial folder with:

``` bash
tar xf UiO66.tar.xz
```

Again, use VMD or Mercury to familiarise yourself with this new structure. A picture of it is given in Figure 3 but you should use the viewer to understand its pore geometry. 
!["UiO-66: just like IRMOF1, it is a porous MOF. Note its different pore geometry from IRMOF1"](https://lh3.googleusercontent.com/OxT8Hbw-B2OF-YdZQQPMRTVHYeOcmZiUAPdx-mUgm2uO_LeUNtChP6p1L5QxlYg-1CZzcn8hJ_5h)

   Figure 3: UiO66: just like IRMOF1, this is a porous metal-organic framework; it can accomodate molecules in its midst. Note how different its pore geometry is from IRMOF1.

This time you only have the `.cif` file at your disposal. You could
type in everything from scratch, but we suggest instead that you copy over the files from your previous calculations in the current folder. You can use the *wildcard* pattern (*) discussed above to select all the files from
`/exports/csce/eddie/chem/groups/Hobday/Jiaying/tutorials/IRMOF1/` at once.

What needs to change in `simulation.input`? Be sure to use the correct
framework name.

There is one other thing that needs to be changed. Like IRMOF1, the unit-cell of UiO66 is cubic but it is only `20.7 Angstroms` along each edge
(IRMOF1 has edge lengths of `25.8 Angstroms`). We are using a cut-off
radius of `12.8 Angstroms`. Is a simulation box of `1*1*1` UiO66 unit-cell big enough ?

If this box size was used, we would disobey the *minimum image convention*. 
This is a convention which stops particles interacting with copies of themselves. So the cut-off distance is always less than half the length of the box.

See here for more on the  [*minimum image convention*]( https://drive.google.com/open?id=1nOPBDGAcufRW0RS70jnDSkuoM2HFJNzb) 

Should you increase the number of unit-cells or should you decrease the cut-off radius?

Once you are confident about the changes you made, be absolutely certain to still be on the computing node and launch the calculation as described before. Once finished, make a note of the average K<sub>H</sub> and interaction energy for each compound.

## Analysing the data

* You should start by comparing the average K<sub>H</sub> obtained for each compound in IRMOF1: which one is the most strongly adsorbed? how does this relate to adsorption energies?

* Take each compound one by one and compare adsorption energies for each  framework: is one framework more strongly-interacting than the other?  What about K<sub>H</sub>? One of the most important factors in adsorption in *microporous* adsorbents (solids which have pores `< 20 Angstroms` in diameter) like IRMOF1 and UiO66 is the pore size. Generally, molecules are more strongly adsorbed in smaller pores. Why do you think this is the case? Do your results agree with this general trend?
  
  To answer these questions, you need to estimate pore sizes. For the time being, you can get a fairly good estimation of it in VMD by measuring the distance between atoms using the `Mouse --> Label --> Bonds` menu and by clicking on any two atoms to get a distance (even if they are non-bonded to each other).  If you chose appropriate atoms, you should be able to get a rough estimate of pore diameters for each structure. In addition, you can use Mercury's void tool ` Display --> Voids --> Ok ` to know the pore volume and visualise it.

* In most applications, more than one compound will be present in the process. For both CO <sub>2</sub> capture (where we try to remove CO <sub>2</sub> from flue gases) and TNT detection, water will be present in the process. We want to selectively adsorb CO <sub>2</sub> or TNT and adsorb as little water as possible. You can use Henry's coefficients to estimate the selectivity S~ab~ between species`a` and `b` at low loading:



   
   $S_\mathbb{ab} \approx \frac{K_\mathrm{H}^\mathrm{a}}{K_\mathrm{H}^\mathrm{b}}$
   


  A selectivity of greater than **1** indicates that component a will be preferentially adsorbed. Based on your results, which MOF is a better
 choice for use in a carbon capture process? What about TNT detection?

* What about equilibration in this case? How many values should we remove to get a good sample?


## Going further

In this tutorial we have used the `TraPPE` (transferable potentials for phase equilibria) models for CO <sub>2</sub> and TNT. These parameters are fitted against experimental *vapour-liquid equilibria* data in the case of CO <sub>2</sub>. For TNT, those data don't exist and so the parameters are those developed for similar compounds.

* note: It is *always* a good idea (read, *necessary*) to identify limitations in your simulation approach!  
    
Instead of the `spc` water model you used earlier, we are using a slightly more complicated model (`tip5p`). The dispersion interactions are still modelled using a **LJ potential** based solely on oxygen--oxygen interactions, but there are some extra pseudo-atoms (called ``Lw`` in this simulation) with electrostatic contributions to try and model the hydrogen bonding more effectively than `spc` water.  For more detail on the concepts, thermodynamics and maths behind Henry's constants as applied to adsorption, you may find Rouquerol's [Adsorption by powders and porous solids](https://www.sciencedirect.com/book/9780125989206/adsorption-by-powders-and-porous-solids) helpful, as well as Myers [Thermodynamics of Adsorption](https://www.seas.upenn.edu/~amyers/CHAPTER21.pdf)  (though it is a bit more heavy-going than the
former!!).

> Written with [StackEdit](https://stackedit.io/).
