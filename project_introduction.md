
# Chemistry 4P Project - Metal Organic Frameworks
$\require{mhchem}$

Supervisors:
	- Dr James Cumby (james.cumby@ed.ac.uk)
	- Dr Claire Hobday (claire.hobday@ed.ac.uk)

Welcome to this 4P project looking at how metal organic frameworks (MOFs) can
be used to address problematic gases within the environment. This document is
designed to give you an overview of the area, and some references that you can 
use to develop your project proposal.

If you have any questions, please get in touch with us!

# Introduction

Metal organic frameworks (MOFs) are a growing class of inorganic compounds known
as framework materials; they exhibit an infinite structure in three dimensions (in
contrast to molecules) but are composed of a mixture of inorganic units (e.g. metal
oxide blocks) and organic linker molecules. This chemical flexibility means that the 
number of known MOFs is growing exponentially, and their physical properties can be 
tuned with relative ease. One particular property of MOFs is that they are often 
porous materials (due to 'voids' within the framework) which can allow them to absorb
gas molecules. In this project, you will use computational techniques to explore these
absorption properties with a view to suggesting MOFs which could solve an environmental
issue.

Gases are all around us, and many of them are unwanted. Examples could include the $\ce{CO_2}$
(and other gases) which are driving global warming, $\ce{NO_{x}}$ compositions which are
a major cause of air pollution or even toxic gases such as $\ce{H2S}$ which is a by product of
power plants. While many of these majhor contributors have been well-studied, there are a number of small-scale 
waste gases which present a significant challenge to the environment or human health. These
situations are where MOFs could see real application; their relatively high complexity and 
difficulty to manufacture in bulk makes it difficult to apply them to e.g. $\ce{CO2}$ capture, but
they could be economically useful for small-scale adsorption tasks. **We encourage you to 
consider gases which have not been well-studied before**.

Within this project you will be able to use a number of computational techniques to 
analyse MOF behaviour. These include simulating the ability of a MOF structure to adsorb 
one or more gas molecules at low or high pressures, as well as structural analysis of 
existing MOFs to interpret which aspects of the bonding and/or composition are important for 
the observed physical properties. You can also use existing experimental adsorption data to help
your analyses. Focus on how, as a team, you can use these different techniques to reach (and justify)
your final conclusions.

As a good introductory paper to computational analysis of MOFs and their adsorption, we recommend you
read [this paper](https://doi.org/10.1039/D0SC01297A).

# Methods

## Gas adsorption calculations

For this project, you have access to two different methods of calculating gas adsorption; Widom insertion
and Grand Canonical Monte Carlo. Both are based around classical dynamics, where interactions between different
atoms are treated as harmonic potentials (i.e. balls and springs) and the overall interaction energy is the 
sum of individual contributions.

Widom insertion is used to calculate the energies of adsorbing a single gas molecule at different places within
a structure, with the position and orientation chosen at random. By repeating this rapid calculation enough times
it becomes possible to determine (a) the favourable interaction sites and (b) the overall likelihood of gas uptake
at low pressures. This simulation method is relatively fast.

Grand Canonical Monte Carlo (GCMC) simulations proceed by randomly moving multiple gas molecules within a structure,
and choosing whether to accept the move based on the total energy change (Monte Carlo algorithm). At each step the chemical
potential is kept constant by adding or removing molecules; this allows the simulation to operate at a well-defined temperature
and pressure. By performing multiple simulations at different pressure conditions, it then becomes possible to compute an entire adsorption 
isotherm, i.e. how much gas is adsorbed at different pressures in a material. This is useful for accurate comparison between
different materials (and with NIST data) but is considerably more time consuming than the Widom insertion method.

## Structural Analysis

The Cambridge Structural Database (CSD) contains structural details of over 2,000,000 organic and inorganic complexes, including
over 10,000 MOF structures. Additionally, it contains tools for searching for, and interpreting crystal structures such as bond
lengths, functional groups or unit cell parameters. Additionally, a Python interface to the database allows you to perform custom 
analyses, for instance calculating the void volumes within MOF structures or determine the 'windows' available for gas molecules
to access these voids. Other tools are reported in [this paper](https://doi.org/10.1039/D0SC01297A).

## Experimental data

The US National Institute of Standards and Technology (NIST) has collated known adsorption isotherms for many different combinations
of MOF structures and adsorbed gases, all of which are freely available [here](https://adsorption.nist.gov/isodb/index.php#home). These
can be explored manually, or alternatively using programmatic access. This allows the data to be numerically compared (either to 
other experimental data or to simulations) and grouped by their characteristics. The data offer a good way to check that simulations
provide experimentally-valid results, with the downside that not all MOFs or adsorbates are experimentally reported.

# Your Initial Tasks

Your first task is to decide on a problem you wish to address, where gas adsorption within MOFs could hold a potential solution. 
This should consider:

- the availability of necessary data to solve the problem
- whether the tools available can be used to provide a solution
- if the problem can be divided between different group members such that you can each
  contribute towards the overall understanding
- whether the problem has been solved already!

Once you have agreed on a problem (in conjunction with JC and CH) then we will show you how to use the different techniques you require.