# Chemistry 4P MOF project

This repository contains materials for use during the Chemistry 4P project on "MOFs for gas adsorption".

## Materials
The repository contains the following materials:

### NIST API introduction
This folder contains a number of files with details on how to use the [NIST Database of Adsorbent Materials](https://adsorption.nist.gov/isodb/index.php#home) through 
an application programming interface (API). 

The best way to view this material is to log on to the [University Noteable service](https://www.ed.ac.uk/information-services/learning-technology/noteable/accessing-noteable)
and then use the '+GitRepo' button to download this whole repository (https://git.ecdf.ed.ac.uk/jcumby/mof_analysis). More details can be found in the 
'Python for Chemistry' Learn course.

Once downloaded, you should then be able to load the NIST_API_intro.ipynb file in Noteable, and run/edit the commands as required. Alternatively, there is also a PDF output 
of the same information, but obviously without the interactive coding.

### Using the Cambridge Structure Database (CSD)
The `Cambridge_structural_database` folder contains details of how to install and interact with the CSD, which is a database of structural information for
~2,000,000 organic molecules and complexes (including MOFs). The files give tutorials on:

- installing the CSD on your computer
- performing simple database searches for compounds using the ConQuest program (such as those containing certain elements)
- Using the Python API to interact with the database in more complex ways


### Using the CSD API to characterise the MOFs
This tutorial can be completed as long as you have installed the CSD.
Download this tutorial to your computer and start the miniconda in the CSD.

For windows:
- Open command prompt window, navigate to the Python_API_2020\miniconda directory and type ‘python’.

For mac: 
- navigate to the miniconda/bin directory and type: `source activate`

This tutorial will show you how to:
- read the MOF database in the CSD
- extract structural parameters from the database in a programatic way
- calculate void space inside MOFs
- calculate window sizes
- export your results as csv files and plot them inside jupyter notebooks.
