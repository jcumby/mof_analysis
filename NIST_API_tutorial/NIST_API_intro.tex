% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[
]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{fancyvrb}
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdftitle={Introduction to NIST Database API},
  pdfauthor={James Cumby},
  hidelinks,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\VerbatimFootnotes % allow verbatim text in footnotes
\usepackage[margin=1in]{geometry}
\usepackage{listings}
\newcommand{\passthrough}[1]{#1}
\lstset{defaultdialect=[5.3]Lua}
\lstset{defaultdialect=[x86masm]Assembler}
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\lstset{
    basicstyle=\ttfamily,
    numbers=left,
    numberstyle=\footnotesize,
    stepnumber=2,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2,
    captionpos=b,
    breaklines=true,
    breakatwhitespace=true,
    breakautoindent=true,
    linewidth=\textwidth,
	language=Python
}

\title{Introduction to NIST Database API}
\author{James Cumby}
\date{September 2019}

\begin{document}
\maketitle

\hypertarget{accessing-the-nistarpa-e-database-of-novel-and-emerging-adsorbent-material-api}{%
\section{Accessing the NIST/ARPA-E Database of Novel and Emerging
Adsorbent Material
API}\label{accessing-the-nistarpa-e-database-of-novel-and-emerging-adsorbent-material-api}}

The NIST Database of Novel and Emerging Materials
(\href{https://adsorption.nist.gov/isodb/index.php\#home}{DNEM})
provides an Application Programming Interface (API) that allows you to
search for and access data using a computer program. This tutorial is
specifically aimed at accessing the API using Python 3, but
documentation for the API can be found
\href{https://adsorption.nist.gov/isodb/index.php\#apis}{here}.

\hypertarget{quick-overview}{%
\subsection{Quick Overview}\label{quick-overview}}

The API can access bibliographic information (containing DOI, title,
material and gas types, \emph{etc}) based on user-defined searches
(\emph{e.g.} by gas type or framework type) or by specific entry names
(\emph{i.e.} search by DOI {[}digital object identifier{]} or title, to
extract a single entry). Isotherm data can be extracted by filename
(which are reported in the linked bibliographic entry).

\hypertarget{getting-started}{%
\subsection{Getting started}\label{getting-started}}

To use Python to access the API, we require the
\href{https://pypi.org/project/requests/}{requests} package to be
installed.\footnote{If not, you may need to use
  \passthrough{\lstinline!pip install requests!} or
  \passthrough{\lstinline!conda install requests!} depending on your
  Python version.} To check that everything is working, try the
following within a python (Jupyter or Ipython) session:

\begin{lstlisting}
import requests

info = requests.get("https://adsorption.nist.gov/api/version.json")
print('API Version: {}'.format(info.json()['version']))
\end{lstlisting}

This should have run and printed a version number as output (3.4657 at
the time of writing).

\begin{description}
\item[What just happened?]
The code above loads the \passthrough{\lstinline!requests!} module, and
then uses it's \passthrough{\lstinline!.get!} method to contact the NIST
API, and ask it for the current API version (../api/version.json). The
information returned is stored in the \passthrough{\lstinline!info!}
variable, which is then processed to find the version number (we will go
through this in more detail later).
\end{description}

\hypertarget{getting-bibliographic-information}{%
\subsection{Getting bibliographic
information}\label{getting-bibliographic-information}}

The API provides a number of ways to search for information records:

\hypertarget{search-by-part-of-the-title-or-doi-for-a-single-record}{%
\subsubsection{Search by (part of the) title or DOI for a single
record}\label{search-by-part-of-the-title-or-doi-for-a-single-record}}

\begin{lstlisting}
# By title or DOI
title = requests.get('https://adsorption.nist.gov/api/biblio/Grand Canonical Transition-Matrix Monte Carlo.json')
DOI = requests.get('https://adsorption.nist.gov/isodb/api/biblio/10.1021/Jp400480q.json')

print('Searched by title: ',title.json()[0]['title'])
print('Searched by DOI: ',DOI.json()[0]['title'])
\end{lstlisting}

\hypertarget{search-by-adsorbent-material-name}{%
\subsubsection{Search by adsorbent material
name}\label{search-by-adsorbent-material-name}}

\begin{lstlisting}
all_CuBTC_MOFs = requests.get('https://adsorption.nist.gov/isodb/api/biblio-material/CuBTC.json')

print('There are {} CuBTC MOF entries in the database'.format(len(all_CuBTC_MOFs.json())))
\end{lstlisting}

\hypertarget{search-by-adsorbate-gas}{%
\subsubsection{Search by adsorbate gas}\label{search-by-adsorbate-gas}}

\begin{lstlisting}
N2_adsorbate_records = requests.get('https://adsorption.nist.gov/isodb/api/biblio-gas/C6H12.json')

print ('Search found {} records containing C6H12 as an adsorbate'.format(len(N2_adsorbate_records.json())))
\end{lstlisting}

Note that these methods can all return multiple records, \emph{e.g.} by
matching just part of the title.

\hypertarget{a-warning}{%
\subsubsection{A warning!}\label{a-warning}}

Be aware that the search by material name or gas is not necessarily
unique. Consider example (3) above;
C\textsubscript{6}H\textsubscript{12} could correpond to either
cyclohexane \emph{or} hex-1-ene (and a few others), and both results are
returned (in fact, there are currently 35 entries for cyclohexane, and 3
for hexene).
\includegraphics[width=0.5\textwidth,height=\textheight]{./isomers.png}\\
For this reason, the database can be searched using the unique
`InChIKey' representation (for gases) or `hashkey' (for materials).
These can be found using the API itself and then used for further
searches. For instance, if we knew we actually wanted cyclohexane in
(3), but had no idea what the InChIKey was:

\begin{lstlisting}
# First, search to get the unique key
cyclohexane = requests.get('https://adsorption.nist.gov/isodb/api/gas/cyclohexane.json')
cyclohexane_inchikey = cyclohexane.json()['InChIKey']

# Second, use that to search the database to give reliable results
cyclohexane_records = requests.get('https://adsorption.nist.gov/isodb/api/biblio-gas/{}.json'.format(cyclohexane_inchikey))

print ('There are {} records containing cyclohexane'.format(len(cyclohexane_records.json())))
\end{lstlisting}

We have searched by name for the unique key, and then use that to
guarentee that we extract the results we want. The database contains a
list of synonyms for different gases, so searching for Hex-1-ene will
give the same InChIKey as if you searched for 1-hexene or
1-C\textsubscript{6}H\textsubscript{12}.

You might want to search for the unique identifier for an adsorbant
material (hashkey), for for instance if the same material is known by
two different names (\emph{e.g.} MOF-5 and IRMOF-1). If so, use the
following API request:

\begin{lstlisting}
MOF5 = requests.get('https://adsorption.nist.gov/matdb/api/material/mof-5.json')
print ('Returned information:')
print (MOF5.text)

print ('\nThe hashkey for MOF-5 (or IRMOF-1) is', MOF5.json()['hashkey'])
\end{lstlisting}

\hypertarget{getting-isotherm-data}{%
\subsection{Getting Isotherm Data}\label{getting-isotherm-data}}

The API methods discussed so far allow you to extract database items by
gases or materials, but only return bibliographic information such as
publication name, year, authors etc. Examining the entry we extracted in
part (1) we can see that the isotherm information is not present,
instead there is a reference to a filename(s):

\begin{lstlisting}
# Using `title` which we created earlier:
print(title.text)
\end{lstlisting}

from this output we can see that this single entry contains information
for \textbf{two} adsorbate gases (carbon dioxide and argon) and
\textbf{three} materials (MWCNT, ZIF-8 and solid CO\textsubscript{2}),
and the resulting isotherms are found in the files
`10.1021Jp400480q.IsothermX' for X = 1 to 8. If we want to get the
actual isotherm data, we need to use another part of the API (note the
slight difference in http address to before):

\begin{lstlisting}
# First, extract the isotherm file names from the bibliographic entry
isotherm_files = title.json()[0]['isotherms']

# Then, if we wanted to get the data for the second file
isotherm2 = requests.get('https://adsorption.nist.gov/isodb/api/isotherm/{}.json'.format(isotherm_files[1]['filename']))        # Remember that Python starts counting from 0, so the second file is number 1 in the list...

print ('The available information in the isotherm file is:')
for key in isotherm2.json().keys():
    print('\t',key)
\end{lstlisting}

The isotherm files provide much more than pressure/adsorption data,
including details of the material and gas(es) involved, the units the
data are reported in and the measurement temperature, amongst others.

In order to plot a simple isotherm curve, we can make use of the
\passthrough{\lstinline!matplotlib!} library (if you are using an
ipython terminal, you may have already imported this).

\begin{lstlisting}
import matplotlib.pyplot as plt

# Convert our json output (from requests) into python dictionaries
iso = isotherm2.json()

# The following looks a bit complicated, but is a quick way to convert the nested dictionaries into a list of numbers for plotting
pressures = [i['pressure'] for i in iso['isotherm_data']]
adsorption = [j['total_adsorption'] for j in iso['isotherm_data']]

plt.plot(pressures, adsorption, marker='o', linestyle='-', c='k', label='Isotherm2')
plt.xlabel('Pressure ({})'.format(iso['pressureUnits']))
plt.ylabel('Total Adsorption ({})'.format(iso['adsorptionUnits']))

# If you want to save the resulting graph, uncomment the following line
# plt.savefig('FILENAME.png')
\end{lstlisting}

As you may have guessed, there is one big drawback to this approach; if
a bibliographic entry contains more than one set of isotherm data there
is no way (from the bibliographic info) to determine which isotherm file
corresponds to which gas/material combination! This seems to be an
oversight from the people at NIST, but we can get round it using a bit
more Python. For instance, if we wanted to find all the ZIF-8 and
CO\textsubscript{2} isotherm files from this same record, we could
construct the following loop to check:

\begin{lstlisting}
for file in title.json()[0]['isotherms']:
    current_isotherm = requests.get('https://adsorption.nist.gov/isodb/api/isotherm/{}.json'.format(file['filename'])).json()
    # Check if the current file contains ZIF-8 and CO2
    if (current_isotherm['adsorbent']['name'] == 'ZIF-8') and ('carbon dioxide' in [i['name'].lower() for i in current_isotherm['adsorbates']]):
        print ( file['filename'], 'contains ZIF-8 and CO2')
    
\end{lstlisting}

This loop will return the isotherm filenames, which you could then use
to extract the data and check for temperature, units etc. If you find
that you need to extract a lot of data repeatedly, it may be better to
write a Python function that can filter it for you. The following
example takes one or more bibliographic entries as input, loops through
their isotherm files and checks to see if the adsorbent/adsorbate
combination matches those specified. The output from the function is a
Python dictionary of matching isotherm filenames with the corresponding
isotherm file content.

\begin{lstlisting}
def filter_isotherms(bibliographic_entry, adsorbent, adsorbate):
    ''' Return a dictionary of isotherms matching the given adsorbent and adsorbate, with the isotherm data included '''
    
    # Check if the bibliographic entry is a dictionary or a list of dictionaries (this assumes that the .json() method has already been run on the requests result
    # This approach allows bibliographic_entry to be either one entry, or a list of entries to check
    if isinstance(bibliographic_entry, dict):
        input_data = [bibliographic_entry]
    elif isinstance(bibliographic_entry, list):
        input_data = bibliographic_entry
    
    matching_files = {}
    
    for file in input_data:
        for iso in file['isotherms']:
            current_isotherm = requests.get('https://adsorption.nist.gov/isodb/api/isotherm/{}.json'.format(iso['filename'])).json()
            # Check if the current file contains adsorbate and adsorbent
            if (current_isotherm['adsorbent']['name'].lower() == adsorbent.lower()) and (adsorbate.lower() in [i['name'].lower() for i in current_isotherm['adsorbates']]):
                matching_files[iso['filename']] = current_isotherm
            
    return matching_files
\end{lstlisting}

This approach avoids having to make too many API requests, which are
inherently quite slow. To use the function, you must specify the input
bibliographic data, adsorbent name and adsorbate gas:

\begin{lstlisting}
MWCNT_data = filter_isotherms(title.json(), 'MWCNT', 'argon')

for dat in MWCNT_data:
    print (dat, 'was collected at', MWCNT_data[dat]['temperature'], 'K')
\end{lstlisting}

\hypertarget{handling-data-with-pandas}{%
\subsection{Handling Data with Pandas}\label{handling-data-with-pandas}}

If you have a number of isotherm curves in a dictionary, it quickly
becomes difficult to process all the data to ensure compatibility
(\emph{i.e.} the same units) and to easily plot the data. One solution
to this is to use the \href{https://pandas.pydata.org/}{Pandas} package,
which adds easy-to-use data structures and analysis tools for
manipulating data (think Excel, but more flexible).

The main way of storing data in Pandas is in a
\passthrough{\lstinline!DataFrame!} object, so the first step is to
manipulate our data into this format. For many data formats (such as
nested dictionaries of information) this is as simple as typing
\passthrough{\lstinline!pd.DataFrame(source\_of\_data)!}, but in our
case (for the isotherm data) we need to do a bit more processing:

\begin{lstlisting}
import pandas as pd

def isotherm_to_dataframe(isotherm_file):
    ''' Convert the information in an isotherm file into a pandas DataFrame '''
    
    # First, extract the isotherm_data. You could just use 'pressure' and 'total_adsorption' at this point, but
    # we want to get the partial adsorbances of different gases (if more than one gas is present)
    data = pd.DataFrame(isotherm_file['isotherm_data'])
    
    # Loop over the different gases in the isotherm, checking that we have
    # extracted the correct data
    for i, gas in enumerate(isotherm_file['adsorbates']):
        gas_name = gas['name']
        gas_data = pd.DataFrame( list(data['species_data'].apply(lambda x: x[i])) )     # Quick way to get the i'th species, and return a DataFrame for it
        
        # Check that we are looking at the correct gas (should be, as long as the database is constructed sensibly)
        assert (gas_data['InChIKey'] == gas['InChIKey']).all()
        
        # Pandas lets us easily add a new column to our data table, making sure the rows are aligned properly
        data[gas_name + '_adsorption'] = gas_data['adsorption']
        data[gas_name + '_composition'] = gas_data['composition']
        
    return data.drop('species_data', axis=1)
\end{lstlisting}

This function will return a pandas DataFrame containing all the
adsorption information from the isotherm file. Unfortunately there is no
way to keep the metadata (temperature, units etc) with the resulting
data, so the best way is to add the new DataFrame object back in to the
original isotherm information as a new `key'. \footnote{Don't worry, you
  can't break the NIST database! You will just be modifying your local
  version of the data.}

\begin{lstlisting}

isotherm = MWCNT_data['10.1021Jp400480q.Isotherm1']

isotherm['dataframe'] = isotherm_to_dataframe( MWCNT_data['10.1021Jp400480q.Isotherm1'] )

# Print the first few rows of the dataframe
isotherm['dataframe'].head()
\end{lstlisting}

As you can see above, pandas displays the data in a clear table format.
It can also be used for some very powerful data manipulation, however.
For example, imagine that we wanted to convert the total adsorption to
different units (for the current isotherm it is in kmol
m\textsuperscript{-3}, but suppose we wanted it in mol
cm\textsuperscript{-3}. We can just add a new column based on the old
one:

\begin{lstlisting}
isotherm['dataframe']['total_adsorption_mol/cm3'] = isotherm['dataframe']['total_adsorption'] * 0.001
isotherm['dataframe'].head()
\end{lstlisting}

In the same way, we can perform arithmetic by combining columns
(\emph{e.g.}
\passthrough{\lstinline!'total\_adsorption' - 'Argon\_adsorption'!},
which in this case should be a column of zeros). We can also extract
individual values from the table using \passthrough{\lstinline!.loc!},
or use some in-built methods in Pandas:

\begin{lstlisting}
# Find the change in adsorption from the minimum value
isotherm['dataframe']['total_adsorption - minimum adsorption'] = isotherm['dataframe']['total_adsorption'] - isotherm['dataframe'].loc[0,'total_adsorption']
# An alternative (perhaps better) way to do this would be to explicitly use the minimum:
#isotherm['dataframe']['total_adsorption - minimum adsorption'] = isotherm['dataframe']['total_adsorption'] - isotherm['dataframe']['total_adsorption'].min()

# Find the change in adsorption at each point
isotherm['dataframe']['delta(abs)'] = isotherm['dataframe']['total_adsorption'].diff()

isotherm['dataframe'].head()
\end{lstlisting}

\hypertarget{plotting-with-pandas}{%
\subsubsection{Plotting with Pandas}\label{plotting-with-pandas}}

Pandas also provides some convenient plotting functions, which make it
really easy to plot graphs. At its simplest:

\begin{lstlisting}
isotherm['dataframe'].plot(x='pressure', y='total_adsorption')
\end{lstlisting}

But we can do much, much more\ldots{}

\begin{lstlisting}
fig, axes = plt.subplots(1,1)

isotherm['dataframe'][['pressure','total_adsorption','Argon_adsorption','delta(abs)']].plot(
    x='pressure',
    secondary_y='delta(abs)',
    ax=axes,
    style=['ko-','g^:','r.--'],
    label=['Total','Argon','$\Delta(total)$'],
)

axes.set_xlim(0.5,0.9)
axes.right_ax.set_ylim(-25,25)
axes.right_ax.set_ylabel('$\Delta(\mathrm{Adsorption})$')

axes.set_xlabel('Pressure ({})'.format(isotherm['pressureUnits']))
axes.set_ylabel('Adsorption ({})'.format(isotherm['adsorptionUnits']))
\end{lstlisting}

For more details about pandas and how to acheive certain things, see
\href{https://pandas.pydata.org/pandas-docs/stable/}{the documentation}
(which is really good, and gives loads of examples)!

\hypertarget{too-much-detail-about-json}{%
\subsection{Too much detail about
JSON}\label{too-much-detail-about-json}}

Throughout this tutorial, we have extracted data from the web using
requests, and then somehow turned it into something useful. In the
middle of this process there is a little method called
\passthrough{\lstinline!.json()!} which seems to have done some magic.
Here we try to explain a bit more about the output from requests.

JSON (Javascript Object Notation) is a way of passing information around
the internet, by giving it a logical structure that can be expressed as
text. If you have played around with the output from
\passthrough{\lstinline!requests!} you will have noticed that it
contains quite a lot of information (try
\passthrough{\lstinline!dir(title)!} to see what is available). The most
useful parts are

\begin{description}
\tightlist
\item[\texttt{title.status\_code}]
This tells you whether the request was successful or not (should = 200
for a good response). Ideally your code should check that the API has
responded correctly
\item[\texttt{title.text}]
This returns the content of the API request as text, which can be useful
for printing/debugging (\emph{i.e.} `print(title.text)
\item[\texttt{title.json()}]
This is the most useful command, and turns the text from the API into
something Python-readable (a combination of dictionaries and lists). As
shown in the previous examples, once you have converted the output to
JSON, you can extract items from a list (\emph{e.g.}
\passthrough{\lstinline!title.json()[0]!}) or retrieve values from a
dictionary based on the key (\emph{e.g.}
\passthrough{\lstinline!isotherm['adsorbent']['name']!})
\end{description}

A general rule is to always run the \passthrough{\lstinline!.json()!}
method on the output from requests, unless you know you don't need to!
If you run into problems like:

\begin{lstlisting}
isotherm[0]
\end{lstlisting}

or

\begin{lstlisting}
title.json()['filename']
\end{lstlisting}

check whether you are handling a list or a dictionary (if unsure, check
with \emph{e.g.} \passthrough{\lstinline!type(isotherm)!})

\end{document}
